import { expect } from "chai";
import { Allocation } from "./moneta.juno";
import { getTestAllocation, getTestExpense } from "./utility.test";

describe("Creating an Allocation", () => {

    describe("With valid Data", () => {

        const testAllocation = getTestAllocation();

        it("Should have valid Name", () => {
            expect(testAllocation.name).to.equal("test allocation");
        });

        it("Should have valid Amount", () => {
            expect(testAllocation.amount).to.equal(100.00);
        });

        it("Should have Zero Spent", () => {
            expect(testAllocation.spent).to.equal(0.00);
        });

        it("Should have Amount as Unspent", () => {
            expect(testAllocation.unspent).to.equal(100.00);
        });

        it("Should have no Expenses", () => {
            expect(testAllocation.expenses.length).to.equal(0);
        });

    });

    describe("With valid Expense Data", () => {

        const testAllocation = new Allocation({
            name: "test allocation",
            amount: 200.00,
            expenses: [
                { id: 0, entryDate: new Date("9/3/1990"), description: "test expense 1", amount: 1.00 },
                { id: 1, entryDate: new Date("9/4/1990"), description: "test expense 2", amount: 2.00 },
                { id: 2, entryDate: new Date("9/5/1990"), description: "test expense 3", amount: 3.00 },
                { id: 3, entryDate: new Date("9/6/1990"), description: "test expense 4", amount: 4.00 },
            ],
        });

        it("Should have Expenses", () => {
            expect(testAllocation.expenses.length).to.equal(4);
        });

        it("Should have Expense total as Spent", () => {
            expect(testAllocation.spent).to.equal(10.00);
        });

        it("Should have Expense total reflected in Unspent", () => {
            expect(testAllocation.unspent).to.equal(190.00);
        });

    });

    describe("With an empty Name", () => {

        it("Should throw an Exception", () => {
            expect(() => {
                const a = new Allocation({
                    name: "",
                    amount: 100.00,
                });
            }).to.throw();
        });

    });

    describe("With a negative Amount", () => {

        it("Should throw an Exception", () => {
            expect(() => {
                const a = new Allocation({
                    name: "test allocation",
                    amount: -100.00,
                });
            }).to.throw();
        });

    });

    describe("With non unique Expense Data", () => {

        it("Should throw an Exception", () => {
            expect(() => {
                const a = new Allocation({
                    name: "test allocation",
                    amount: 200.00,
                    expenses: [
                        { id: 0, entryDate: new Date("9/3/1990"), description: "test expense 1", amount: 1.00 },
                        { id: 0, entryDate: new Date("9/4/1990"), description: "test expense 2", amount: 2.00 },
                        { id: 1, entryDate: new Date("9/5/1990"), description: "test expense 3", amount: 3.00 },
                        { id: 2, entryDate: new Date("9/6/1990"), description: "test expense 4", amount: 4.00 },
                    ],
                });
            }).to.throw();
        });

    });

});

describe("Updating an Allocation", () => {

    describe("With a valid Name", () => {

        const testAllocation = getTestAllocation();
        let eventFired = false;
        testAllocation.on("allocation.verifyName", () => eventFired = true);

        testAllocation.name = "new name";

        it("Should have a valid Name", () => expect(testAllocation.name).to.equal("new name"));
        it("Should fire a VerifyName Event", () => expect(eventFired).to.be.true);

    });

    describe("With a valid Amount", () => {

        const testAllocation = getTestAllocation();
        let eventFired = false;
        testAllocation.on("allocation.verifyAmount", () => eventFired = true);

        testAllocation.amount = 500.00;

        it("Should have a valid Amount", () => expect(testAllocation.amount).to.equal(500.00));
        it("Should fire a VerifyAmount Event", () => expect(eventFired).to.be.true);

    });

    describe("With an empty Name", () => {

        const testAllocation = getTestAllocation();
        let eventFired = false;
        testAllocation.on("allocation.verifyName", () => eventFired = true);

        it("Should throw an Exception", () => {
            expect(() => {
                testAllocation.name = "";
            }).to.throw();
        });
        it("Should not fire a VerifyName Event", () => expect(eventFired).to.be.false);

    });

    describe("With a negative Amount", () => {

        const testAllocation = getTestAllocation();
        let eventFired = false;
        testAllocation.on("allocation.verifyAmount", () => eventFired = true);

        it("Should throw and Exception", () => {
            expect(() => {
                testAllocation.amount = -200.00;
            }).to.throw();
        });
        it("Should not fire a VerifyAmount Event", () => expect(eventFired).to.be.false);

    });

});

describe("Adding an Expense", () => {

    describe("With valid Data", () => {

        const testAllocation = getTestAllocation({ amount: 100.00 });
        const testExpense = getTestExpense({ amount: 3.00 });
        let eventFired = false;
        testAllocation.on("allocation.verifyNewExpense", () => eventFired = true);

        testAllocation.record(testExpense);

        it("Should be added to Allocation", () => expect(testAllocation.expenses.length).to.equal(1));
        it("Should increase total Spent", () => expect(testAllocation.spent).to.equal(3.00));
        it("Should decrease total Unspent", () => expect(testAllocation.unspent).to.equal(97.00));
        it("Should fire a VerifyExpense Event", () => expect(eventFired).to.be.true);

    });

    describe("With duplicate Id", () => {

        const testAllocation = getTestAllocation({ amount: 100.00 });
        const testExpense = getTestExpense({ amount: 3.00 });

        testAllocation.record(testExpense);

        let eventFired = false; // setup event handler to catch second record call
        testAllocation.on("allocation.verifyNewExpense", () => eventFired = true);

        it("Should throw an Exception", () => expect(() => testAllocation.record(testExpense)).to.throw());
        it("Should not be added to Allocation", () => expect(testAllocation.expenses.length).to.equal(1));
        it("Should not change total Spent", () => expect(testAllocation.spent).to.equal(3.00));
        it("Should not change total Unspent", () => expect(testAllocation.unspent).to.equal(97.00));
        it("Should not fire a VerifyExpense Event", () => expect(eventFired).to.be.false);

    });

});

describe("Removing an Expense", () => {

    describe("That is existing", () => {

        const testAllocation = getTestAllocation({ amount: 100.00 });
        const testExpense = getTestExpense({ id: 1, amount: 3.00 });
        testAllocation.record(testExpense);
        testAllocation.record(getTestExpense({ id: 2, amount: 4.00 }));

        testAllocation.delete(testExpense.id);

        it("Should be removed from Allocation", () => expect(testAllocation.expenses.length).to.equal(1));
        it("Should decrease total Spent", () => expect(testAllocation.spent).to.equal(4.00));
        it("Should increase total Unspent", () => expect(testAllocation.unspent).to.equal(96.00));

    });

    describe("That doesn't exist", () => {

        const testAllocation = getTestAllocation({ amount: 100.00 });
        testAllocation.record(getTestExpense({ id: 1, amount: 3.00 }));
        testAllocation.record(getTestExpense({ id: 2, amount: 4.00 }));

        testAllocation.delete(3);

        it("Should not be removed from Allocation", () => expect(testAllocation.expenses.length).to.equal(2));
        it("Should not change total Spent", () => expect(testAllocation.spent).to.equal(7.00));
        it("Should not change total Unspent", () => expect(testAllocation.unspent).to.equal(93.00));

    });

});
