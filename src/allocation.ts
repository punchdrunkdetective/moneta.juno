import { EventEmitter } from "events";
import { Expense } from "./expense";

export class Allocation extends EventEmitter {

    public get name(): string { return this.NAME; }
    public set name(newName: string) {
        if (!this.validName(newName)) { throw new Error(`invalid name: ${newName}`); }
        this.emit("allocation.verifyName", newName);
        this.NAME = newName;
    }

    public get amount(): number { return this.AMOUNT; }
    public set amount(newAmount: number) {
        if (!this.validAmount(newAmount)) { throw new Error(`invalid amount: ${newAmount}`); }
        this.emit("allocation.verifyAmount", newAmount);
        this.AMOUNT = newAmount;
    }

    public get expenses(): Expense[] { return this.EXPENSES; }
    public get spent(): number { return this.EXPENSES.map((e) => e.amount).reduce((acc, curr) => acc + curr, 0); }
    public get unspent(): number { return this.amount - this.spent; }

    private NAME: string;
    private AMOUNT: number;
    private EXPENSES: Expense[];

    constructor(data: any) {
        super();

        this.EXPENSES = [];
        this.name = data.name;
        this.amount = data.amount;

        if (data.expenses) {
            data.expenses.map((e: any) => new Expense(e)).forEach((e) => this.record(e));
        }
    }

    public record(exps: Expense): void {
        if (this.EXPENSES.some((e) => e.id === exps.id)) { throw new Error(`expense: ${exps.id} already exists`); }
        this.emit("allocation.verifyNewExpense", exps);
        this.EXPENSES.push(exps);
    }

    public delete(expsId: number): void {
        this.EXPENSES = this.EXPENSES.filter((e) => e.id !== expsId);
    }

    private validName(name: string): boolean {
        return name !== "";
    }

    private validAmount(amount: number): boolean {
        return Math.abs(amount - 0.00) <= 0.001 || amount > 0.00;
    }

}
