import { expect } from "chai";
import { Allocation, Budget, Expense } from "./moneta.juno";
import { getTestAllocation, getTestBudget, getTestExpense } from "./utility.test";

describe("Verifying a Budget", () => {

    describe("When updating the Budget Amount", () => {

        let testBudget: Budget = null;
        let newAmount: number = 0.0;

        beforeEach(() => {
            testBudget = getTestBudget({ amount: 200.00 });
            testBudget.allocate(getTestAllocation({ amount: 150.00 }));
        });

        describe("With a under allocated amount.", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    newAmount = 100.00;
                    testBudget.amount = newAmount;
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should throw an exception", () => expect(thrownEx).to.not.be.null);
            it("Should not update Budget Amount", () => expect(testBudget.amount).to.not.equal(newAmount));

        });

    });

    describe("When updating the Budget StartDate", () => {

        let testBudget: Budget = null;
        let newStartDate: Date = null;

        beforeEach(() => {
            testBudget = getTestBudget({ startDate: new Date("12/1/2019"), endDate: new Date("12/31/2019") });
            testBudget.allocate(getTestAllocation());
            testBudget.allocations[0].record(getTestExpense({ entryDate: new Date("12/15/2019") }));
        });

        describe("To an Expense EntryDate", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    newStartDate = new Date("12/15/2019");
                    testBudget.setDateRange(newStartDate, testBudget.endDate);
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should not throw an exception", () => expect(thrownEx).to.be.null);
            it("Should update Budget StartDate", () => expect(testBudget.startDate).to.eql(newStartDate));

        });

        describe("After an Expense EntryDate", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    newStartDate = new Date("12/16/2019");
                    testBudget.setDateRange(newStartDate, testBudget.endDate);
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should throw an exception", () => expect(thrownEx).to.not.be.null);
            it("Should not update Budget StartDate", () => expect(testBudget.startDate).to.not.eql(newStartDate));

        });

    });

    describe("When updating the Budget EndDate", () => {

        let testBudget: Budget = null;
        let newEndDate: Date = null;

        beforeEach(() => {
            testBudget = getTestBudget({ startDate: new Date("12/1/2019"), endDate: new Date("12/31/2019") });
            testBudget.allocate(getTestAllocation());
            testBudget.allocations[0].record(getTestExpense({ entryDate: new Date("12/15/2019") }));
        });

        describe("To an Expense EntryDate", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    newEndDate = new Date("12/15/2019");
                    testBudget.setDateRange(testBudget.startDate, newEndDate);
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should not throw an exception", () => expect(thrownEx).to.be.null);
            it("Should update Budget EndDate", () => expect(testBudget.endDate).to.eql(newEndDate));

        });

        describe("Before an Expense EntryDate", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    newEndDate = new Date("12/14/2019");
                    testBudget.setDateRange(testBudget.startDate, newEndDate);
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should throw an exception", () => expect(thrownEx).to.not.be.null);
            it("Should not update Budget EndDate", () => expect(testBudget.endDate).to.not.eql(newEndDate));

        });

    });

});

describe("Verifying an Allocation", () => {

    describe("When updating the Allocation Name", () => {

        let testBudget: Budget = null;
        let testAllocation: Allocation = null;
        let newName: string = "";

        beforeEach(() => {
            testBudget = getTestBudget();
            testBudget.allocate(getTestAllocation({ name: "allocation 1" }));
            testBudget.allocate(getTestAllocation({ name: "allocation 2" }));
        });

        describe("With a valid Name", () => {

            beforeEach(() => {
                testAllocation = testBudget.allocations[1];
                newName = "valid name";
                testAllocation.name = newName;
            });

            it("Should have valid Name", () => expect(testAllocation.name).to.equal(newName));

        });

        describe("With same Name", () => {

            beforeEach(() => {
                testAllocation = testBudget.allocations[1];
                newName = testAllocation.name;
                testAllocation.name = newName;
            });

            it("Should have same Name", () => expect(testAllocation.name).to.equal(newName));

        });

        describe("With a duplicate Name", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    testAllocation = testBudget.allocations[1];
                    newName = testBudget.allocations[0].name;
                    testAllocation.name = newName;
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should throw an exception", () => expect(thrownEx).to.not.be.null);
            it("Should not update Allocation Name", () => expect(testAllocation.name).to.not.equal(newName));

        });

    });

    describe("When updating the Allocation Amount", () => {

        let testBudget: Budget = null;
        let testAllocation: Allocation = null;
        let newAmount: number = 0.0;

        beforeEach(() => {
            testBudget = getTestBudget({ amount: 200.00 });
            testBudget.allocate(getTestAllocation({ amount: 150.00 }));
        });

        describe("With a valid Amount", () => {

            beforeEach(() => {
                testAllocation = testBudget.allocations[0];
                newAmount = 175.00;
                testAllocation.amount = newAmount;
            });

            it("Should have valid Amount", () => expect(testAllocation.amount).to.equal(newAmount));

        });

        describe("With same Amount", () => {

            beforeEach(() => {
                testAllocation = testBudget.allocations[0];
                newAmount = testAllocation.amount;
                testAllocation.amount = newAmount;
            });

            it("Should have same Amount", () => expect(testAllocation.amount).to.equal(newAmount));

        });

        describe("With a over allocation", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    testAllocation = testBudget.allocations[0];
                    newAmount = 250.00;
                    testAllocation.amount = newAmount;
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should throw an exception", () => expect(thrownEx).to.not.be.null);
            it("Should not update Allocation Amount", () => expect(testAllocation.amount).to.not.equal(newAmount));

        });

    });

});

describe("Verifying an Expense", () => {

    describe("When adding an Expense to a Budget Allocation", () => {

        let testBudget: Budget = null;
        let testAllocation: Allocation = null;

        beforeEach(() => {
            testBudget = getTestBudget({ startDate: new Date("12/1/2019"), endDate: new Date("12/31/2019") });
            testAllocation = getTestAllocation();
            testBudget.allocate(testAllocation);
        });

        describe("With a valid EntryDate", () => {

            beforeEach(() => {
                testAllocation.record(getTestExpense({ entryDate: new Date("12/5/2019") }));
            });

            it("Should add Expense", () => expect(testAllocation.expenses.length).to.equal(1));

        });

        describe("With a EntryDate at Budget DateRange min", () => {

            beforeEach(() => {
                testAllocation.record(getTestExpense({ entryDate: new Date("12/1/2019") }));
            });

            it("Should add Expense", () => expect(testAllocation.expenses.length).to.equal(1));

        });

        describe("With a EntryDate at Budget DateRange max", () => {

            beforeEach(() => {
                testAllocation.record(getTestExpense({ entryDate: new Date("12/31/2019") }));
            });

            it("Should add Expense", () => expect(testAllocation.expenses.length).to.equal(1));

        });

        describe("With a EntryDate below Budget DateRange", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    testAllocation.record(getTestExpense({ entryDate: new Date("11/30/2019") }));
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should throw an Exception", () => expect(thrownEx).to.not.be.null);
            it("Should not add Expense", () => expect(testAllocation.expenses.length).to.equal(0));

        });

        describe("With a EntryDate above Budget DateRange", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    testAllocation.record(getTestExpense({ entryDate: new Date("1/1/2020") }));
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should throw an Exception", () => expect(thrownEx).to.not.be.null);
            it("Should not add Expense", () => expect(testAllocation.expenses.length).to.equal(0));

        });

    });

    describe("When updating the Expense EntryDate after allocation", () => {

        let testBudget: Budget = null;
        let testAllocation: Allocation = null;
        let testExpense: Expense = null;
        let newEntryDate: Date = null;

        beforeEach(() => {

            testBudget = getTestBudget({
                startDate: new Date("12/1/2019"),
                endDate: new Date("12/31/2019"),
                amount: 100.00,
            });
            testAllocation = getTestAllocation({ amount: 100.00 });
            testExpense = getTestExpense({ entryDate: new Date("12/5/2019"), amount: 12.04 });

            testBudget.allocate(testAllocation);
            testAllocation.record(testExpense);

        });

        describe("With a valid EntryDate", () => {

            beforeEach(() => {
                newEntryDate = new Date("12/13/2019");
                testExpense.entryDate = newEntryDate;
            });

            it("Should have valid EntryDate", () => expect(testExpense.entryDate).to.eql(newEntryDate));

        });

        describe("With a EntryDate at Budget DateRange min", () => {

            beforeEach(() => {
                newEntryDate = new Date("12/1/2019");
                testExpense.entryDate = newEntryDate;
            });

            it("Should have valid EntryDate", () => expect(testExpense.entryDate).to.eql(newEntryDate));

        });

        describe("With a EntryDate at Budget DateRange max", () => {

            beforeEach(() => {
                newEntryDate = new Date("12/31/2019");
                testExpense.entryDate = newEntryDate;
            });

            it("Should have valid EntryDate", () => expect(testExpense.entryDate).to.eql(newEntryDate));

        });

        describe("With a EntryDate below Budget DateRange", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    newEntryDate = new Date("11/30/2019");
                    testExpense.entryDate = newEntryDate;
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should throw an Exception", () => expect(thrownEx).to.not.be.null);
            it("Should not update the Expense EntryDate", () => expect(testExpense.entryDate).to.not.eql(newEntryDate));

        });

        describe("With a EntryDate above Budget DateRange", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    newEntryDate = new Date("1/1/2020");
                    testExpense.entryDate = newEntryDate;
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should throw an Exception", () => expect(thrownEx).to.not.be.null);
            it("Should not update the Expense EntryDate", () => expect(testExpense.entryDate).to.not.eql(newEntryDate));

        });

    });

    describe("When updating the Expense EntryDate before Allocation", () => {

        let testBudget: Budget = null;
        let testAllocation: Allocation = null;
        let testExpense: Expense = null;
        let newEntryDate: Date = null;

        beforeEach(() => {

            testBudget = getTestBudget({
                startDate: new Date("12/1/2019"),
                endDate: new Date("12/31/2019"),
                amount: 100.00,
            });
            testAllocation = getTestAllocation({ amount: 100.00 });
            testExpense = getTestExpense({ entryDate: new Date("12/5/2019"), amount: 12.04 });

            testAllocation.record(testExpense);
            testBudget.allocate(testAllocation);

        });

        describe("With a valid EntryDate", () => {

            beforeEach(() => {
                newEntryDate = new Date("12/13/2019");
                testExpense.entryDate = newEntryDate;
            });

            it("Should have valid EntryDate", () => expect(testExpense.entryDate).to.eql(newEntryDate));

        });

        describe("With a EntryDate at Budget DateRange min", () => {

            beforeEach(() => {
                newEntryDate = new Date("12/1/2019");
                testExpense.entryDate = newEntryDate;
            });

            it("Should have valid EntryDate", () => expect(testExpense.entryDate).to.eql(newEntryDate));

        });

        describe("With a EntryDate at Budget DateRange max", () => {

            beforeEach(() => {
                newEntryDate = new Date("12/31/2019");
                testExpense.entryDate = newEntryDate;
            });

            it("Should have valid EntryDate", () => expect(testExpense.entryDate).to.eql(newEntryDate));

        });

        describe("With a EntryDate below Budget DateRange", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    newEntryDate = new Date("11/30/2019");
                    testExpense.entryDate = newEntryDate;
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should throw an Exception", () => expect(thrownEx).to.not.be.null);
            it("Should not update the Expense EntryDate", () => expect(testExpense.entryDate).to.not.eql(newEntryDate));

        });

        describe("With a EntryDate above Budget DateRange", () => {

            let thrownEx: Error = null;
            beforeEach(() => {
                try {
                    newEntryDate = new Date("1/1/2020");
                    testExpense.entryDate = newEntryDate;
                } catch (e) {
                    thrownEx = e;
                }
            });

            it("Should throw an Exception", () => expect(thrownEx).to.not.be.null);
            it("Should not update the Expense EntryDate", () => expect(testExpense.entryDate).to.not.eql(newEntryDate));

        });

    });

    describe("When adding an Allocation that already has the Expense", () => {

        let testBudget: Budget = null;
        let testAllocation: Allocation = null;
        let testExpense: Expense = null;

        beforeEach(() => {
            testBudget = getTestBudget({
                startDate: new Date("12/1/2019"),
                endDate: new Date("12/31/2019"),
                amount: 100.00,
            });
            testAllocation = getTestAllocation({ amount: 100.00 });
        });

        describe("With a valid EntryDate", () => {

            beforeEach(() => {
                testExpense = getTestExpense({ entryDate: new Date("12/5/2019"), amount: 12.04 });
                testAllocation.record(testExpense);
            });

            it("Should not throw an Exception", () => expect(() => testBudget.allocate(testAllocation)).to.not.throw());
        });

        describe("With a EntryDate at Budget DateRange min", () => {

            beforeEach(() => {
                testExpense = getTestExpense({ entryDate: new Date("12/1/2019"), amount: 12.04 });
                testAllocation.record(testExpense);
            });

            it("Should not throw an Exception", () => expect(() => testBudget.allocate(testAllocation)).to.not.throw());

        });

        describe("With a EntryDate at Budget DateRange max", () => {

            beforeEach(() => {
                testExpense = getTestExpense({ entryDate: new Date("12/31/2019"), amount: 12.04 });
                testAllocation.record(testExpense);
            });

            it("Should not throw an Exception", () => expect(() => testBudget.allocate(testAllocation)).to.not.throw());

        });

        describe("With a EntryDate below Budget DateRange", () => {

            let thrownEx: Error = null;
            beforeEach(() => {

                testExpense = getTestExpense({ entryDate: new Date("11/30/2019"), amount: 12.04 });
                testAllocation.record(testExpense);

                try {
                    testBudget.allocate(testAllocation);
                } catch (e) {
                    thrownEx = e;
                }

            });

            it("Should throw an Exception", () => expect(thrownEx).to.not.be.null);
            it("Should not add Allocation to Budget", () => expect(testBudget.allocations.length).to.equal(0));

        });

        describe("With a EntryDate above Budget DateRange", () => {

            let thrownEx: Error = null;
            beforeEach(() => {

                testExpense = getTestExpense({ entryDate: new Date("1/1/2020"), amount: 12.04 });
                testAllocation.record(testExpense);

                try {
                    testBudget.allocate(testAllocation);
                } catch (e) {
                    thrownEx = e;
                }

            });

            it("Should throw an Exception", () => expect(thrownEx).to.not.be.null);
            it("Should not add Allocation to Budget", () => expect(testBudget.allocations.length).to.equal(0));

        });

    });

});
