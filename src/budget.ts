import { Allocation } from "./allocation";

export class Budget {

    public get name(): string { return this.NAME; }
    public set name(newName: string) {
        if (!this.validName(newName)) { throw new Error(`invalid name: ${newName}`); }
        this.NAME = newName;
    }

    public get startDate(): Date { return this.START_DATE; }
    // validation done in "set date range" method, consider making "date range" a tuple

    public get endDate(): Date { return this.END_DATE; }
    // validation done in "set date range" method, consider making "date range" a tuple

    public get amount(): number { return this.AMOUNT; }
    public set amount(newAmount: number) {
        if (!this.validAmount(newAmount)) { throw new Error(`invalid amount: ${newAmount}`); }
        this.AMOUNT = newAmount;
    }

    public get allocations(): Allocation[] { return this.ALLOCATIONS; }

    public get fundsAllocated(): number {
        return this.ALLOCATIONS.map((a) => a.amount).reduce((acc, curr) => acc + curr, 0);
    }

    public get fundsAvailable(): number { return this.amount - this.fundsAllocated; }

    private START_DATE: Date;
    private END_DATE: Date;
    private AMOUNT: number;
    private NAME: string;
    private ALLOCATIONS: Allocation[];

    constructor(data: any) {

        this.ALLOCATIONS = [];

        this.name = data.name;
        this.amount = data.amount;

        // "date range" must be set at the same time due to validation resitrictions, consider making
        // "date range" a tuple type.
        this.setDateRange(data.startDate, data.endDate);

        if (data.allocations) {
            data.allocations.map((a: any) => new Allocation(a)).forEach((a) => this.allocate(a));
        }

    }

    public setDateRange(newStartDate: Date, newEndDate: Date): void {

        if (!this.validDateRange(newStartDate, newEndDate)) { throw new Error("invalid date range"); }

        this.allocations.forEach((a) => {
            a.expenses.forEach((e) => {
                if (e.entryDate < newStartDate || e.entryDate > newEndDate) {
                    const errMsg = `new date range ${newStartDate} - ${newEndDate} invalidates expense ${e.entryDate}`;
                    throw new Error(errMsg);
                }
            });
        });

        this.START_DATE = newStartDate;
        this.END_DATE = newEndDate;

    }

    public allocate(allc: Allocation): void {

        if (this.allocationExists(allc.name)) {
            throw new Error(`allocation: ${allc.name} already exists`);
        }

        if (this.fundsAvailable - allc.amount < 0) {
            throw new Error(`allocation: ${allc.name} exceeds funds available`);
        }

        allc.on("allocation.verifyName", (newName) => {
            if (allc.name !== newName && this.allocationExists(newName)) {
                throw new Error(`allocation: ${newName} already exists`);
            }
        });

        allc.on("allocation.verifyAmount", (newAmount) => {
            if (this.fundsAvailable + allc.amount - newAmount < 0) {
                throw new Error(`allocation: ${newAmount} exceeds funds available`);
            }
        });

        allc.on("allocation.verifyNewExpense", (newExps) => {
            this.validateExpenseEntryDate(newExps.entryDate);
            newExps.on("expense.verifyEntryDate", (newEntryDate) => this.validateExpenseEntryDate(newEntryDate));
        });

        allc.expenses.forEach((exps) => {
            this.validateExpenseEntryDate(exps.entryDate);
            exps.on("expense.verifyEntryDate", (newEntryDate) => this.validateExpenseEntryDate(newEntryDate));
        });

        this.ALLOCATIONS.push(allc);

    }

    public unallocate(allcName: string): void {
        this.ALLOCATIONS = this.ALLOCATIONS.filter((a) => a.name !== allcName);
    }

    private validName(name: string): boolean {
        return name !== "";
    }

    private validDateRange(startDate: Date, endDate: Date): boolean {
        return startDate <= endDate;
    }

    private isZero(val: number): boolean {
        // NOTE: you may be taking to many precautions around zero.
        return Math.abs(val - 0.00) <= 0.001;
    }

    private validAmount(amount: number): boolean {

        return this.isZero(amount)
            ? this.isZero(this.fundsAllocated)
            : amount > 0.00 && amount >= this.fundsAllocated;

    }

    private validateExpenseEntryDate(entryDate: Date): void {
        if (entryDate < this.startDate || entryDate > this.endDate) {
            throw new Error(`Expense: ${entryDate} outside Budget date range`);
        }
    }

    private allocationExists(name: string): boolean {
        return this.ALLOCATIONS.some((a) => a.name === name);
    }

}
