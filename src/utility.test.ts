import { Allocation, Budget, Expense } from "./moneta.juno";

export function getTestBudget(initData?: any): Budget {
    initData = initData || {};
    return new Budget({
        name: initData.name || "test budget",
        startDate: initData.startDate || new Date("8/7/1990"),
        endDate: initData.endDate || new Date("9/3/1991"),
        amount: initData.amount || 500.00,
    });
}

export function getTestAllocation(initData?: any): Allocation {
    initData = initData || {};
    return new Allocation({
        name: initData.name || "test allocation",
        amount: initData.amount || 100.00,
    });
}

export function getTestExpense(initData?: any): Expense {
    initData = initData || {};
    return new Expense({
        id: initData.id || 0,
        entryDate: initData.entryDate || new Date("9/3/1990"),
        description: initData.description || "test expense",
        amount: initData.amount || 1.00,
    });
}
