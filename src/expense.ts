import { EventEmitter } from "events";

export class Expense extends EventEmitter {

    public get id(): number { return this.ID; }

    public get entryDate(): Date { return this.ENTRY_DATE; }
    public set entryDate(newEntryDate: Date) {
        this.emit("expense.verifyEntryDate", newEntryDate);
        this.ENTRY_DATE = newEntryDate;
    }

    public get description(): string { return this.DESCRIPTION; }
    public set description(newDescription: string) {
        this.emit("expense.verifyDescription", newDescription);
        this.DESCRIPTION = newDescription;
    }

    public get amount(): number { return this.AMOUNT; }
    public set amount(newAmount: number) {
        if (!this.validAmount(newAmount)) { throw new Error(`invalid amount: ${newAmount}`); }
        this.emit("expense.verifyAmount", newAmount);
        this.AMOUNT = newAmount;
    }

    private ID: number;
    private ENTRY_DATE: Date;
    private DESCRIPTION: string;
    private AMOUNT: number;

    constructor(data: any) {
        super();
        this.ID = data.id;
        this.entryDate = data.entryDate;
        this.description = data.description;
        this.amount = data.amount;
    }

    private validAmount(amount: number): boolean {
        return Math.abs(amount - 0.00) <= 0.001 || amount > 0.00;
    }

}
