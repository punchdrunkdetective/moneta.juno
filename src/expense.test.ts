import { expect } from "chai";
import { Expense } from "./moneta.juno";
import { getTestExpense } from "./utility.test";

describe("Creating an Expense", () => {

    describe("With valid Data", () => {

        const testExpense = getTestExpense();

        it("Should have valid Id", () => expect(testExpense.id).to.equal(0));
        it("Should have valid EntryDate", () => expect(testExpense.entryDate).to.eql(new Date("9/3/1990")));
        it("Should have valid Description", () => expect(testExpense.description).to.equal("test expense"));
        it("Should have valid Amount", () => expect(testExpense.amount).to.equal(1.00));

    });

    describe("With a negative Amount", () => {

        it("Should throw an Exception", () => {
            expect(() => {
                const e = new Expense({
                    id: 0,
                    entryDate: new Date("9/3/1990"),
                    description: "test expense",
                    amount: -1.00,
                });
            }).to.throw();
        });

    });

});

describe("Updating an Expense", () => {

    describe("With a valid EntryDate", () => {

        const testExpense = getTestExpense();
        let eventFired = false;
        testExpense.on("expense.verifyEntryDate", () => eventFired = true);

        testExpense.entryDate = new Date("8/7/1990");

        it("Should have a valid EntryDate", () => expect(testExpense.entryDate).to.eql(new Date("8/7/1990")));
        it("Should fire a VerifyEntryDate Event", () => expect(eventFired).to.be.true);

    });

    describe("With a valid Description", () => {

        const testExpense = getTestExpense();
        let eventFired = false;
        testExpense.on("expense.verifyDescription", () => eventFired = true);

        testExpense.description = "updated description";

        it("Should have a valid Description", () => expect(testExpense.description).to.equal("updated description"));
        it("Should fire a VerifyDescription Event", () => expect(eventFired).to.be.true);

    });

    describe("With a valid Amount", () => {

        const testExpense = getTestExpense();
        let eventFired = false;
        testExpense.on("expense.verifyAmount", () => eventFired = true);

        testExpense.amount = 2.00;

        it("Should have a valid Amount", () => expect(testExpense.amount).to.equal(2.00));
        it("Should fire a VerifyAmount Event", () => expect(eventFired).to.be.true);

    });

    describe("With a negative Amount", () => {

        const testExpense = getTestExpense();
        let eventFired = false;
        testExpense.on("expense.verifyAmount", () => eventFired = true);

        it("Should throw an Exception", () => {
            expect(() => {
                testExpense.amount = -2.00;
            }).to.throw();
        });
        it("Should have a valid Amount", () => expect(testExpense.amount).to.equal(1.00));
        it("Should not fire a VerifyAmount Event", () => expect(eventFired).to.be.false);

    });

});
