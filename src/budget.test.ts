import { expect } from "chai";
import { Budget } from "./moneta.juno";
import { getTestAllocation, getTestBudget } from "./utility.test";

// TODO: add tests for creating a budget with expense data (allocation with expenses)

describe("Creating a Budget", () => {

    describe("With valid Data", () => {

        const testBudget = getTestBudget();

        it("Should have valid Name", () => expect(testBudget.name).to.equal("test budget"));

        it("Should have valid Date Range", () => {
            expect(testBudget.startDate).to.eql(new Date("8/7/1990"));
            expect(testBudget.endDate).to.eql(new Date("9/3/1991"));
        });

        it("Should have valid Amount", () => expect(testBudget.amount).to.equal(500.00));

        it("Should have Zero Funds Allocated", () => expect(testBudget.fundsAllocated).to.equal(0.00));

        it("Should have Amount as Funds Available", () => expect(testBudget.fundsAvailable).to.equal(500.00));

        it("Should have no Allocations", () => expect(testBudget.allocations.length).to.equal(0));

    });

    describe("With valid Allocation Data", () => {

        const testBudget = new Budget({
            name: "test budget",
            startDate: new Date("8/7/1990"),
            endDate: new Date("9/3/1991"),
            amount: 500.00,
            allocations: [
                { name: "Food", amount: 100.00 },
                { name: "Gas", amount: 100.00 },
                { name: "Grocery", amount: 100.00 },
                { name: "Misc", amount: 100.00 },
            ],
        });

        it("Should have Allocations", () => expect(testBudget.allocations.length).to.equal(4));

        it("Should have Allocation total as Funds Allocated", () => expect(testBudget.fundsAllocated).to.equal(400.00));

        it("Should have Allocation total refected in Funds Available", () => {
            expect(testBudget.fundsAvailable).to.equal(100.00);
        });

    });

    describe("With an empty Name", () => {

        it("Should throw an Exception", () => {
            expect(() => {
                const b = new Budget({
                    name: "",
                    startDate: new Date("8/7/1990"),
                    endDate: new Date("9/3/1991"),
                    amount: 500.00,
                });
            }).to.throw();
        });

    });

    describe("With a single Date", () => {

        const testBudget = getTestBudget({startDate: new Date("1/1/2020"), endDate: new Date("1/1/2020")});

        it("Should have valid Date Range", () => {
            expect(testBudget.startDate).to.eql(new Date("1/1/2020"));
            expect(testBudget.endDate).to.eql(new Date("1/1/2020"));
        });

    });

    describe("With an invalid Date Range", () => {

        it("Should throw an Exception", () => {
            expect(() => {
                const b = new Budget({
                    name: "test budget",
                    startDate: new Date("9/3/1991"),
                    endDate: new Date("8/7/1990"),
                    amount: 500.00,
                });
            }).to.throw();
        });

    });

    describe("With a negative Amount", () => {

        it("Should throw an Exception", () => {
            expect(() => {
                const b = new Budget({
                    name: "test budget",
                    startDate: new Date("8/7/1990"),
                    endDate: new Date("9/3/1991"),
                    amount: -500.00,
                });
            }).to.throw();
        });

    });

    describe("With a zero Amount", () => {

        const testBudget = new Budget({
            name: "test budget",
            startDate: new Date("8/7/1990"),
            endDate: new Date("9/3/1991"),
            amount: 0,
        });

        it("Should have zero as Amount", () => expect(testBudget.amount).to.equal(0.00));

    });

    describe("With non unique Allocation Data", () => {

        it("Should throw an Exception", () => {
            expect(() => {
                const b = new Budget({
                    name: "test budget",
                    startDate: new Date("8/7/1990"),
                    endDate: new Date("9/3/1991"),
                    amount: 500.00,
                    allocations: [
                        { name: "Food", amount: 100.00 },
                        { name: "Food", amount: 50.00 },
                        { name: "Grocery", amount: 100.00 },
                        { name: "Misc", amount: 100.00 },
                    ],
                });
            }).to.throw();
        });

    });

    describe("With over Allocation", () => {

        it("Should throw an Exception", () => {
            expect(() => {
                const b = new Budget({
                    name: "test budget",
                    startDate: new Date("8/7/1990"),
                    endDate: new Date("9/3/1991"),
                    amount: 500.00,
                    allocations: [
                        { name: "Food", amount: 100.00 },
                        { name: "Gas", amount: 300.00 },
                        { name: "Grocery", amount: 100.00 },
                        { name: "Misc", amount: 100.00 },
                    ],
                });
            }).to.throw();
        });

    });

});

describe("Updating a Budget", () => {

    describe("With a valid Name", () => {

        const testBudget = getTestBudget();
        testBudget.name = "new name";
        it("Should have valid Name", () => expect(testBudget.name).to.equal("new name"));

    });

    describe("With a valid Date Range", () => {

        const testBudget = getTestBudget();
        testBudget.setDateRange(new Date("10/1/2019"), new Date("10/31/2019"));
        it("Should have valid Date Range", () => {
            expect(testBudget.startDate).to.eql(new Date("10/1/2019"));
            expect(testBudget.endDate).to.eql(new Date("10/31/2019"));
        });

    });

    describe("With a valid Amount", () => {

        const testBudget = getTestBudget();
        testBudget.amount = 300.00;
        it("Should have valid Amount", () => expect(testBudget.amount).to.equal(300.00));

    });

    describe("With an empty Name", () => {

        const testBudget = getTestBudget();
        it("Should throw an Exception", () => expect(() => testBudget.name = "").to.throw());

    });

    describe("With a single Date", () => {

        const testBudget = getTestBudget();
        testBudget.setDateRange(new Date("1/1/2020"), new Date("1/1/2020"));

        it("Should have valid Date Range", () => {
            expect(testBudget.startDate).to.eql(new Date("1/1/2020"));
            expect(testBudget.endDate).to.eql(new Date("1/1/2020"));
        });

    });

    describe("With an invalid Date Range", () => {

        const testBudget = getTestBudget();
        const startDate = new Date("9/3/1991");
        const endDate = new Date("8/7/1990");
        it("Should throw an Exception", () => expect(() => testBudget.setDateRange(startDate, endDate)).to.throw());

    });

    describe("With a negative Amount", () => {

        const testBudget = getTestBudget();
        it("Should throw an Exception", () => expect(() => testBudget.amount = -300.00).to.throw());

    });

    describe("With a zero Amount", () => {

        const testBudget = getTestBudget({ amount: 500 });
        testBudget.amount = 0;
        it("Should have zero as Amount", () => expect(testBudget.amount).to.equal(0.00));

    });

});

describe("Adding an Allocation", () => {

    describe("With valid Data", () => {

        const testBudget = getTestBudget({ amount: 500.00 });
        const testAllocation = getTestAllocation({ amount: 100.00 });

        testBudget.allocate(testAllocation);

        it("Should be added to the Budget", () => expect(testBudget.allocations.length).to.equal(1));
        it("Should increase total Funds Allocated", () => expect(testBudget.fundsAllocated).to.equal(100.00));
        it("Should decrease total Funds Available", () => expect(testBudget.fundsAvailable).to.equal(400.00));

    });

    describe("With duplicate Name", () => {

        const testBudget = getTestBudget({ amount: 500.00 });
        const testAllocation = getTestAllocation({ name: "test allocation", amount: 100.00 });

        testBudget.allocate(testAllocation);

        it("Should throw an Exception", () => expect(() => testBudget.allocate(testAllocation)).to.throw());
        it("Should not be added to the Budget", () => expect(testBudget.allocations.length).to.equal(1));
        it("Should increase total Funds Allocated", () => expect(testBudget.fundsAllocated).to.equal(100.00));
        it("Should decrease total Funds Available", () => expect(testBudget.fundsAvailable).to.equal(400.00));

    });

    describe("With to large an Amount", () => {

        const testBudget = getTestBudget({ amount: 500.00 });
        const testAllocation = getTestAllocation({ name: "test allocation", amount: 100.00 });

        testBudget.allocate(testAllocation);

        it("Should throw an Exception", () => {
            return expect(() => {
                return testBudget.allocate(getTestAllocation({ name: "too large", amount: 500.00 }));
            }).to.throw();
        });
        it("Should not be added to the Budget", () => expect(testBudget.allocations.length).to.equal(1));
        it("Should not change total Funds Allocated", () => expect(testBudget.fundsAllocated).to.equal(100.00));
        it("Should not change total Funds Available", () => expect(testBudget.fundsAvailable).to.equal(400.00));

    });

});

describe("Removing an Allocation", () => {

    describe("That is existing", () => {

        const testBudget = getTestBudget({ amount: 500.00 });
        const testAllocation = getTestAllocation({ name: "test allocation", amount: 100.00 });
        testBudget.allocate(testAllocation);
        testBudget.allocate(getTestAllocation({ name: "allocation 2", amount: 200.00 }));

        testBudget.unallocate(testAllocation.name);

        it("Should be removed from the Budget", () => expect(testBudget.allocations.length).to.equal(1));
        it("Should decrease total Funds Allocated", () => expect(testBudget.fundsAllocated).to.equal(200.00));
        it("Should increase total Funds Available", () => expect(testBudget.fundsAvailable).to.equal(300.00));

    });

    describe("That doesn't exist", () => {

        const testBudget = getTestBudget({ amount: 500.00 });
        testBudget.allocate(getTestAllocation({ name: "test allocation", amount: 100.00 }));
        testBudget.allocate(getTestAllocation({ name: "allocation 2", amount: 200.00 }));

        testBudget.unallocate("fake allocation");

        it("Should be removed from the Budget", () => expect(testBudget.allocations.length).to.equal(2));
        it("Should not change total Funds Allocated", () => expect(testBudget.fundsAllocated).to.equal(300.00));
        it("Should not change total Funds Available", () => expect(testBudget.fundsAvailable).to.equal(200.00));

    });

});
