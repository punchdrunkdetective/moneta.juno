# Moneta Juno 


## What is this repository for? 

moneta.juno is a library I am writing to help me understand javascript and nodejs. It is a personal project of mine that l primarily use as a test bed to familiarize myself with a number of technologies and design patterns. It is a re-implementation of a C# library I wrote with the same purpose, and contains a number of classes that represent different money transactions you would typically make in a bank account or record in a checkbook.


## How do I get set up? 

npm install

npm run start - for active development, runs a watching linter and test runner for real time feed back

npm run compile - transpiles the project into es5 in a lib folder

(see package.json for more scripts)


## Contribution guidelines 

TBD


## Who do I talk to? 

Me! at punchdrunkdetective@gmail.com

